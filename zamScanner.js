var net    = require('net')
  , Socket = net.Socket
  , prompt = require('prompt')
  , async  = require('async')

var zamScan = exports

/**
 * Description
 * @method findAPortInUse
 * @param {} startPort
 * @param {} endPort
 * @param {} host
 * @param {} callback
 * @return 
 */
zamScan.findAPortInUse = function(startPort, endPort, host, callback) {
  findAPortWithStatus('abierto', startPort, endPort, host, callback)
}

/**
 * Description
 * @method findAPortNotInUse
 * @param {} startPort
 * @param {} endPort
 * @param {} host
 * @param {} callback
 * @return 
 */
zamScan.findAPortNotInUse = function(startPort, endPort, host, callback) {
  findAPortWithStatus('cerrado', startPort, endPort, host, callback)
}


/**
 * Description
 * @method checkPortStatus
 * @param {} port
 * @param {} options
 * @param {} callback
 * @return 
 */
zamScan.checkPortStatus = function(port, options, callback) {
  if (typeof options === 'string') {
    
    options = {host: options}
  }

  var host = options.host || '127.0.0.1'
  var timeout = options.timeout || 400

  var socket = new Socket()
    , status = null
    , error = null

  
  socket.on('connect', function() {
    status = 'abierto'
    if(port === 80){
      console.log(http)
    }
    socket.destroy()
  })

 
  socket.setTimeout(timeout)
  socket.on('timeout', function() {
    status = 'cerrado'
    error = new Error('Timeout (' + timeout + 'ms) occurred waiting for ' + host + ':' + port + ' to be available')
    socket.destroy()
  })

  socket.on('error', function(exception) {
    error = exception
    status = 'cerrado'
  })

  socket.on('close', function() {
    callback(error, status)
  })

  socket.connect(port, host)
}

/**
 * Description
 * @method findAPortWithStatus
 * @param {} status
 * @param {} startPort
 * @param {} endPort
 * @param {} host
 * @param {} callback
 * @return 
 */
function findAPortWithStatus(status, startPort, endPort, host, callback) {
  endPort = endPort || 65535
  var foundPort = false
  var numberOfPortsChecked = 0
  var port = startPort

  /**
   * Description
   * @method hasFoundPort
   * @return LogicalExpression
   */
  var hasFoundPort = function() {
    return foundPort || numberOfPortsChecked === (endPort - startPort + 1)
  }

  /**
   * Description
   * @method checkNextPort
   * @param {} callback
   * @return 
   */
  var checkNextPort = function(callback) {
    portscanner.checkPortStatus(port, host, function(error, statusOfPort) {
      numberOfPortsChecked++
      if (statusOfPort === status) {
        foundPort = true
        callback(error)
      }
      else {
        port++
        callback(null)
      }
    })
  }

  async.until(hasFoundPort, checkNextPort, function(error) {
    if (error) {
      callback(error, port)
    }
    else if (foundPort) {
      callback(null, port)
    }
    else {
      callback(null, false)
    }
  })
}

prompt.start();

/**
 * Description
 * @method evalPort
 * @param {} port
 * @param {} status
 * @return 
 */
function evalPort(port, status){
  switch(port){
  
        case "1":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n" + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es tcpmux un Multiplexador de servicios de puertos TCP');
          break;
        
        case "5":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es rje una entrada de trabajo remota ');
          break;
        
        case "7":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es echo un Servicio echo ');
          break;

        case "9":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es discard un Servicio nulo para la evaluación de conexiones ');
          break;

        case "11":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es systat un Servicio de estado del sistema para listar los puertos conectados ');
          break;

        case "13":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es daytime un Envía la fecha y la hora al puerto solicitante ');
          break;

        case "17":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es qotd en él se Envía la cita del día al host conectado ');
          break;

        case "18":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es msp un Protocolo de envío de mensajes ');
          break;

        case "19":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es chargen un Servicio de generación de caracteres; envía flujos infinitos de caracteres ');
          break;

        case "20":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es ftp-data un Puerto de datos FTP ');
          break;

        case "21":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es ftp un Puerto del Protocolo de transferencia de archivos (FTP); algunas veces utilizado por el Protocolo de servicio de archivos (FSP). ');
          break;


        case "22":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es ssh un Servicio de shell seguro (SSH) ');
          break;


        case "23":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es telnet donde corre El servicio Telnet ');
          break;


        case "25":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es smtp un Protocolo simple de transferencia de correo (SMTP) ');
          break;


        case "37":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es time un Protocolo de hora (Time Protocol) ');
          break;


        case "39":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es rlp un Protocolo de ubicación de recursos ');
          break;


        case "42":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es nameserver un Servicio de nombres de Internet ');
          break;


        case "43":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es nicname un Servicio de directorio WHOIS ');
          break;


        case "49":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es tacacs la Terminal Access Controller Access Control System para el acceso y autenticación basado en TCP/IP ');
          break;


        case "50":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es re-mail-ck un Protocolo de verificación de correo remoto ');
          break;


        case "53":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es domain  Servicios de nombres de dominio (tales como BIND) ');
          break;


        case "63":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es whois++ el cual es un Servicio extendidos WHOIS ');
          break;


        case "67":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es bootps, Los servicios del Protocolo Bootstrap o de inicio (BOOTP); también usado por los servicios del protocolo de configuración dinámica de host (DHCP). ');
          break;


        case "68":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es bootpc un Cliente bootstrap (BOOTP); también usado por el protocolo de configuración dinámica de host (DHCP) ');
          break;


        case "69":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es tftp un Protocolo de transferencia de archivos triviales (TFTP) ');
          break;


        case "70":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es gopher ayuda a la Búsqueda y recuperación de documentos de Internet Gopher ');
          break;


        case "71":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es netrjs-1 un Servicio de trabajos remotos ');
          break;

        case "72":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es netrjs-1 un Servicio de trabajos remotos ');
          break;


        case "73":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es netrjs-1 un Servicio de trabajos remotos');
          break;


        case "79":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es finger un Servicio Finger para información de contacto de usuarios ');
          break;


        case "80":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es http un Protocolo de transferencia de hipertexto (HTTP) para los servicios del World Wide Web (WWW) ');
          break;


        case "88":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es kerberos un Sistema de autenticación de redes Kerberos ');
          break;


        case "95":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es supdup una Extensión del protocolo Telnet ');
          break;


        case "110":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es ');
          break;


        case "111":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es sunrpc');
          break;


        case "123":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NTP Protocolo de sincronización de tiempo');
          break;


        case "135":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es epmap');
          break;


        case "137":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NetBIOS Servicio');
          break;


        case "138":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NetBIOS Servicio');
          break;


        case "139":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NetBIOS Servicio');
          break;


        case "143":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es IMAP4 Internet Message Access Protocol (E-mail)');
          break;


        case "161":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es SNMP Simple Network Management Protocol');
          break;


        case "162":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es SNMP-trap');
          break;


        case "177":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es XDMCP Protocolo de gestión de displays en X11');
          break;


        case "389":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es LDAP Protocolo de acceso ligero a Bases de Datos');
          break;


        case "443":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es HTTPS/SSL usado para la transferencia segura de páginas web');
          break;


        case "445":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Microsoft-DS (Active Directory, compartición en Windows, gusano Sasser, Agobot)');
          break;


        case "465":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es SMTP Sobre SSL. Utilizado para el envío de correo electrónico (E-mail)');
          break;


        case "500":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es IPSec ISAKMP, Autoridad de Seguridad Local');
          break;


        case "512":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es exec');
          break;


        case "513":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Rlogin');
          break;


        case "514":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es syslog usado para logs del sistema');
          break;


        case "520":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es RIP Routing Information Protocol (Protocolo de Información de Enrutamiento)');
          break;


        case "591":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es FileMaker 6.0');
          break;


        case "631":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es CUPS sistema de impresión de Unix');
          break;


        case "666":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es identificación de Doom para jugar sobre TCP');
          break;


        case "993":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es IMAP4 sobre SSL (E-mail)');
          break;


        case "995":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es POP3 sobre SSL (E-mail)');
          break;


        case "1080":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es SOCKS Proxy');
          break;


        case "1337":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' suele usarse en máquinas comprometidas o infectadas');
          break;


        case "1352":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es IBM Lotus Notes/Domino RCP');
          break;


        case "1433":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Microsoft-SQL-Server');
          break;


        case "1434":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Microsoft-SQL-Monitor');
          break;


        case "1494":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Citrix MetaFrame Cliente ICA');
          break;


        case "1512":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es WINS Windows Internet Naming Service');
          break;


        case "1521":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es el Oracle listener por defecto');
          break;


        case "1701":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Enrutamiento y Acceso Remoto para VPN con L2TP.');
          break;


        case "1720":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es H.323');
          break;


        case "1723":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es un Enrutamiento y Acceso Remoto para VPN con PPTP.');
          break;


        case "1761":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Novell Zenworks Remote Control utility');
          break;


        case "1863":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es MSN Messenger');
          break;


        case "1935":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es FMS Flash Media Server');
          break;


        case "2049":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NFS Archivos del sistema de red');
          break;


        case "2082":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es cPanel puerto por defecto');
          break;


        case "2083":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es cPanel puerto por defecto SSL');
          break;


        case "2086":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Web Host Manager puerto por defecto');
          break;


        case "2427":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Cisco MGCP');
          break;


        case "3030":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NetPanzer');
          break;


        case "3074":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Xbox Live');
          break;


        case "3128":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es HTTP usado por web caches y por defecto en Squid cache o por NDL-AAS');
          break;


        case "3306":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es MySQL sistema de gestión de bases de datos');
          break;


        case "3389":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es el RDP (Remote Desktop Protocol) Terminal Server ');
          break;


        case "3396":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Novell agente de impresión NDPS');
          break;


        case "3690":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Subversion (sistema de control de versiones)');
          break;


        case "4662":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es eMule (aplicación de compartición de ficheros)');
          break;

        case "4672":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es eMule (aplicación de compartición de ficheros)');
          break;


        case "4899":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es RAdmin (Remote Administrator), herramienta de administración remota (normalmente troyanos)');
          break;


        case "5000":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Universal plug-and-play');
          break;


        case "5190":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Session Initiation Protocol (SIP)');
          break;


        case "5222":
          console.log('El puerto ' + port + ' se encuentra ' + status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es AOL y AOL Instant Messenger');
          break;


        case "5223":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Jabber/XMPP conexión de cliente');
          break;


        case "5269":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Jabber/XMPP conexión de servidor');
          break;


        case "5432":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es PostgreSQL sistema de gestión de bases de datos');
          break;


        case "5517":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Setiqueue proyecto SETI@Home');
          break;


        case "5631":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es PC-Anywhere protocolo de escritorio remoto');
          break;


        case "5632":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es PC-Anywhere protocolo de escritorio remoto');
          break;


        case "5400":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (usado sobre HTTP)');
          break;


        case "5500":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (usado sobre HTTP)');
          break;


        case "5600":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (usado sobre HTTP)');
          break;


        case "5700":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (usado sobre HTTP)');
          break;


        case "5800":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (usado sobre HTTP)');
          break;


        case "5900":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es VNC protocolo de escritorio remoto (conexión normal)');
          break;


        case "6000":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es X11 usado para X-windows');
          break;


        case "6112":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Blizzard');
          break;


        case "6129":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Dameware Software conexión remota');
          break;


        case "6346":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Gnutella compartición de ficheros (Limewire, etc.)');
          break;


        case "6667":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es IRC IRCU Internet Relay Chat');
          break;


        case "6881":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es BitTorrent puerto por defecto');
          break;


        case "7100":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Servidor de Fuentes X11');
          break;


        case "8000":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es iRDMI por lo general, usado erróneamente en sustitución de 8080. También utilizado en el servidor de streaming ShoutCast.');
          break;


        case "8080":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es HTTP HTTP-ALT. Tomcat lo usa como puerto por defecto.');
          break;


        case "8118":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es privoxy');
          break;


        case "9009":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Pichat peer-to-peer chat server');
          break;


        case "9898":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Gusano Dabber (troyano/virus)');
          break;


        case "10000":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Webmin (Administración remota web)');
          break;


        case "19226":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Panda SecurityPuerto de comunicaciones de Panda Agent.');
          break;


        case "12345":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es NetBus(troyano/virus)');
          break;


        case "31337":
          console.log('El puerto ' + port + ' se encuentra '+ status + "\n");
          console.log('El servicio que corre en el puerto ' + port + ' es Back Orifice herramienta de administración remota (por lo general troyanos)');
          break;


        default:
          console.log('El puerto ' + port + ' se encuentra '+ status + ' y no se sabe que servicio puede estar corriendo');
          break;
        }
}

console.log("###################################################################");
console.log("#######                Bienvenido A                     ###########");
console.log("#######                  zamScan                        ###########");
console.log("#######               un portscanner                    ###########");
console.log("#######               desarrollado por                  ###########");
console.log("#######                Goalkeeper112                    ###########");
console.log("###################################################################");


/**
 * Description
 * @method zamEscaneo
 * @param {} err
 * @param {} status
 * @return 
 */
function zamEscaneo(err, status){
  if(err){
    throw err;
  } 
  prompt.get(['host', 'puerto'], function(err, result){
    if(err){
      return onErr(err);
    }
    var host = result.host;
    var port = result.puerto;
    zamScan.checkPortStatus(port, host, function(err, status){
      console.log("El escaneo al puerto "+ port +" a sido completado con exito");
      if(status === "abierto"){
        evalPort(port ,status);
      }else{
        console.log("El puerto " + port + " se encuentra " + status + " y por eso no se pudo determinar el servicio que corre en él");
      }
      });
  });
}

/**
 * Description
 * @method continuar
 * @return 
 */
function continuar(){
  console.log("¿Desea realizar otro escaneo(yes/no)?");
  prompt.get(['continuar'], function(err, result){
    if(err){
      throw err;
    }
    if(result.continuar == "yes" || result.continuar == "Yes" || result.continuar == "y"){
      if(err){
        throw err;
      }
      zamEscaneo();
    }else{
      console.log("zamScan es desarrollado por Goalkeeper112");
    }
  });
 }
 
zamEscaneo();
setTimeout(function(){
  continuar();
}, 15000);