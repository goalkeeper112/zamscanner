ZamScanner es un escaneador de puertos individual muy eficaz desarrollado bajo la plataforma de NODE.JS. Su uso es muy sencillo y está orientado a la utilización en pentesting y técnicas derivadas, basta con instalarlo de forma global de la siguiente manera:

	$ npm install zamscanner

Luego de instalarlo en el directorio deseado ya podran ejecutarlo, al correrlo el les pedira digitar el host a escanear y luego el puerto, ZamScanner al detectar que un puerto se encuentra abierto es capaz de detectar no de forma exacta el servicio que puedes estar corriendo en el mismo.

NOTA: Zamscanner está pensado para escanear máquinas de la red local su uso contra páginas web no es muy eficaz.

host: "nombre de dominio" or "XXX.XXX.XXX.XXX"
port: "80" or "21" or ...